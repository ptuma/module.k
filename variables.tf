variable "name" {
  description = "Name of the RDS parameter group"
  type        = string
}

variable "family" {
  description = "The family of the DB parameter group"
  type        = string
}

variable "parameters" {
  description = "A list of DB parameters (map) to apply"
  type        = list(map(string))
  default     = []
}

variable "tags" {
  description = "A map of tags to add to domain."
  type        = map(string)
  default     = {}
}

variable "description" {
  description = "Description of the DB parameter group to create"
  type        = string
  default     = ""
}