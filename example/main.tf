provider "aws" {
  region = "eu-central-1"
}

module "db-parameter-group" {
  source = "../"

  family = "postgres11"
  name = "test-postgres11"
  description = "test group"

  parameters = [
    {
      name  = "autovacuum"
      value = 1
    },
    {
      name  = "client_encoding"
      value = "utf8"
    }
  ]

  tags = {
    Project     = "test-project"
    Environment = "test"
  }


}